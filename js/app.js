"use strict"

function initMap() {
    const uluru = { lat: 50.3850861, lng: 30.4703182};
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 17,
        center: uluru,
    });

    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
        animation: google.maps.Animation.BOUNCE,

        // icon: image,
    });
    const descriptionWindow = new google.maps.InfoWindow({
        content: `<h2>Voodoo</h2>
                    <p>137 Glasgow St., Unit 115<br>Kitchener, 
                    ON N2G 4X8<br>Ukraine</p><br>
                    <div class="map__phone--wrapper">
                        <img class="map__phone--image" src="/image/phone.png"/>
                        <span class="map__phone">1-800-480-9597</span>
                    </div>
                    <div class="map__email--wrapper">
                        <img class="map__email--image" src="/image/mail.png"/>
                        <span class="map__email">info@voodoo.com</span>
                    </div>

                `
    });
    descriptionWindow.open(map, marker);


}



window.initMap = initMap;

function selectChange() {
const selectTags = document.querySelector("select");
selectTags.addEventListener('click', (event) => {
    console.log('ChooseSelect');
    });
};
selectChange();